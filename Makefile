CC=g++

# Folders
BIN=bin/
OBJECTS=objects/
EIGENPATH=/home/martin/usr

# ============================= Tests ==================================

equation:
	# Folder structure
	mkdir -p $(BIN)tests
	mkdir -p $(OBJECTS)tests
	# Compiling
	$(CC) -Wall -c -o $(OBJECTS)tests/equation.o -g tests/equation/equation.cpp
	# Linking
	$(CC) -Wall -o $(BIN)tests/equation.x -g $(OBJECTS)tests/equation.o

worldTest: world
	# Folder structure
	mkdir -p $(BIN)tests
	mkdir -p $(OBJECTS)tests
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)tests/world.o -g tests/frogger/world.cpp
	# Linking
	$(CC) -Wall -o $(BIN)tests/world.x -g $(OBJECTS)tests/world.o $(OBJECTS)frogger/world.o $(OBJECTS)ai/neuralnetwork.o

eigenTest:
	# Folder structure
	mkdir -p $(BIN)tests
	mkdir -p $(OBJECTS)tests
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)tests/eigen.o tests/ai/eigen.cpp
	# Linking
	$(CC) -Wall -o $(BIN)tests/eigen.x -g $(OBJECTS)tests/eigen.o

neuralnetworkTest: neuralnetwork
	# Folder structure
	mkdir -p $(BIN)tests
	mkdir -p $(OBJECTS)tests
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)tests/neuralnetwork.o tests/ai/neuralnetwork.cpp
	# Linking
	$(CC) -Wall -o $(BIN)tests/neuralnetwork.x -g $(OBJECTS)tests/neuralnetwork.o $(OBJECTS)ai/neuralnetwork.o

neuralnetworkTest2: neuralnetwork
	# Folder structure
	mkdir -p $(BIN)tests
	mkdir -p $(OBJECTS)tests
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)tests/neuralnetwork2.o tests/ai/neuralnetwork2.cpp
	# Linking
	$(CC) -Wall -o $(BIN)tests/neuralnetwork2.x -g $(OBJECTS)tests/neuralnetwork2.o $(OBJECTS)ai/neuralnetwork.o

# ============================ Genetics ================================


# ============================== Frogger ===============================

world: neuralnetwork
	# Folder structure
	mkdir -p $(OBJECTS)frogger/
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)frogger/world.o -g src/frogger/world.cpp

# ================================ AI ==================================

neuralnetwork:
	# Folder structure
	mkdir -p $(OBJECTS)ai/
	# Compiling
	$(CC) -I $(EIGENPATH) -Wall -c -o $(OBJECTS)ai/neuralnetwork.o src/ai/neuralnetwork.cpp

# =============================== Misc =================================

clean:
	rm -rf $(BIN)
	rm -rf $(OBJECTS)
