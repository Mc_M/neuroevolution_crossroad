#include<iostream>
#include "../../include/world.h"
#include<vector>
#include <random>
#include <cstdlib>
#include <string>

int main(void) {
    
    // Create outdir
    std::string outdir = "TEST_DATA";
    std::string command = "mkdir -p "+outdir;
    system(command.c_str());
    
    // Create world
    std::vector< std::size_t > topology{7, 6};
    Frogger::World w(50, 80, 10, 10, 10, 0.6, topology, AI::NeuralNetwork::ACTIVATION_RELU, 1253);
        
    std::uniform_real_distribution<double> unif(-0.5, 0.5);
    std::default_random_engine re;
    
    double r = -1.0;
    for (int i=0; i<w.genomeLength(); i++) {
        r = unif(re);
        w.setGene(i, r);
    }

    // Advance it for NN times
    int NN = 500;
    for (int nn=0; nn<NN; nn++) {
        w.renderData(outdir, "");
        w.advance();
    }
    
    return 0;
    
    // TODO: Test that the decisions are the exact same after the cars are at the same
    // point if the player is at the same point.
}
