#include <iostream>
#include <vector>
#include <cmath>
#include <memory>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <random>
#include "../../include/individual.h"
#include "../../include/population.h"
#include "../../include/selection.h"
#include "../../include/crossover.h"
#include "../../include/mutation.h"
#include "../../include/geneticalgorithm.h"

// =====================================================================
// 							Equation class
// =====================================================================

class Equation : public Genetics::Individual<int> {

	public:

                static int ctr;

		// Constructors
		Equation(int x0, int x1, int x2, int x3,
			 int c0, int c1, int c2, int c3,
			 int rhs, bool verbose) {
			x0_ = x0;
			x1_ = x1;
			x2_ = x2;
			x3_ = x3;
			c0_ = c0;
			c1_ = c1;
			c2_ = c2;
			c3_ = c3;
			rhs_ = rhs;
                        ctr += 1;
                        id_ = ctr;
                        verbose_ = verbose;
                        if (verbose_) {
                            std::cout << "Equation constructed." << std::endl;
                        }
		};

		// Destructor
		~Equation() {
                    if (verbose_) {
			std::cout << "Destructor of Equation called!" << std::endl;
                    }
		}

		// Function members
		std::vector<int> getX() {
			return std::vector<int>{x0_, x1_, x2_, x3_};
		}

		std::vector<int> getC() {
			return std::vector<int>{c0_, c1_, c2_, c3_};
		}

		int getRhs() {
			return rhs_;
		}

		// Interface functions

		double fitness() {
			std::vector<int> x = getX();
			std::vector<int> c = getC();
			// Compute fObjective
			float fObjective = 0.0;
			for (std::size_t ii=0; ii<genomeLength(); ii++) {
				fObjective += c[ii]*x[ii];
			}
			fObjective -= rhs_;
			fObjective = std::abs(fObjective);
                        if (verbose_) {
                            std::cout << "fitness() called" << std::endl;
                        }
			// Compute fitness
			return 1/(1+fObjective);
		}

		std::size_t genomeLength() {
			return 4;
		}

		void setGene(int index, int gene) {
			if (index == 0) {
				x0_ = gene;
			} else if (index == 1) {
				x1_ = gene;
			} else if (index == 2) {
				x2_ = gene;
			} else if (index == 3) {
				x3_ = gene;
			}
		}

		int getGene(int index) {
			int res = -1;
			if (index == 0) {
				res = x0_;
			} else if (index == 1) {
				res = x1_;
			} else if (index == 2) {
				res = x2_;
			} else if (index == 3) {
				res = x3_;
			}
			return res;
		}

		std::string print() {
			std::vector<int> x = getX();
			std::vector<int> c = getC();
			std::ostringstream res;
			res << "{" << id_ << "} " << getRhs() << " == "
				  << "[ " << c[0] << " ] * ( " << x[0] << " ) "
				  << "+ [ " << c[1] << " ] * ( " << x[1] << " ) "
				  << "+ [ " << c[2] << " ] * ( " << x[2] << " ) "
				  << "+ [ " << c[3] << " ] * ( " << x[3] << " )";
			return res.str();
		}

		static std::shared_ptr< Equation > getRandomEquationPtr(std::default_random_engine &gen,
                                                                        std::uniform_int_distribution<> &dist,
                                                                        int c0, int c1, int c2, int c3, int rhs,
                                                                        bool verbose) {
                    return std::make_shared<Equation>(dist(gen), dist(gen), dist(gen), dist(gen),
                                                      c0, c1, c2, c3, rhs, verbose);
                }

	private:

		int x0_, x1_, x2_, x3_;
		int c0_, c1_, c2_, c3_;
		int rhs_;
                bool verbose_;
                int id_;

};

std::ostream &operator<<(std::ostream &os, Equation &eq) {
	return os << eq.print();
}

int Equation::ctr = 0;

// =====================================================================
//                                 main
// =====================================================================

int main(void) {

        // ========================= Genetic Algorithm =========================

        Genetics::GeneticAlgorithmSettings settings;
        settings.tournamentSize             = 5;
        settings.nElite                     = 10;
        settings.numberOfGenerations        = 200;
        settings.seed                       = 0;
        settings.nPoint                     = 1;
        settings.mutationProbability        = 0.25;
        settings.width                      = 4.0;
        settings.randomGenerator.seed(settings.seed);

        // ============================ Population =============================

        // Create population
        std::size_t cap = 100;
        std::shared_ptr< Genetics::Population<int> > popPtr = std::make_shared<Genetics::Population<int>>(cap);

        // Fill population
        std::uniform_int_distribution<> dist(-100, 100);
        int c0  = 1;
        int c1  = 1;
        int c2  = 1;
        int c3  = 1;
        int rhs = 4;
        for (std::size_t i=0; i<cap; i++) {
            popPtr->addIndividual(Equation::getRandomEquationPtr(settings.randomGenerator, dist, c0, c1, c2, c3, rhs, false));
        }

        // ============================ Operators ==============================

        std::function<void(std::shared_ptr< Genetics::Population<int> >,
                           std::size_t,
                           std::vector<std::size_t> &,
                           Genetics::GeneticAlgorithmSettings &)> selectionFunction = Genetics::Selection::tournamentSelection<int>;

        std::function<void(std::size_t,
                           std::vector<int> &,
                           std::vector<int> &,
                           Genetics::GeneticAlgorithmSettings &)> crossoverFunction = Genetics::Crossover::npoint<int>;

        std::function<void(int &,
                           Genetics::GeneticAlgorithmSettings &)> mutationFunction = Genetics::Mutation::intGaussian;

        // ========================= Genetic Algorithm =========================

        Genetics::GeneticAlgorithm<int> ga(popPtr, settings, selectionFunction, crossoverFunction, mutationFunction);

        std::ofstream of;
        of.open ("performance.dat");
        of << "# gen min avg max\n";

        for (std::size_t ii=0; ii<settings.numberOfGenerations; ii++) {
            std::cout << popPtr->highestFitness() << std::endl;
            of << ii << " " << popPtr->lowestFitness() << " " << popPtr->averageFitness() << " " << popPtr->highestFitness() << "\n";
            ga.advance();
        }

        of.close();

        popPtr->updateRanking();
        for (std::size_t ii=0; ii<popPtr->size(); ii++) {
            std::cout << popPtr->getFittestIndividual(ii)->fitness() << " -> " << popPtr->getFittestIndividual(ii)->print() << std::endl;
        }

        return 0;

}
