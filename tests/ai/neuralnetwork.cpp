#include<iostream>
#include "../../include/neuralnetwork.h"
#include<vector>

int main(void) {
    
    std::vector<std::size_t> nrNeurons{3,2,1};
    std::cout << "Topology: ";
    for (auto nr : nrNeurons) {
        std::cout << nr << " ";
    }
    std::cout << std::endl;
    
    int act = AI::NeuralNetwork::ACTIVATION_LINEAR;
    bool verb = true;
    bool debug = true;
    
    AI::NeuralNetwork nn(nrNeurons, act, verb, debug);
    
    // Set test case input neurons
    nn.setInputNeurons(1, 3);
    nn.setInputNeurons(2, 2);
    nn.setInputNeurons(3, 4);
    
    // Set test case weights
    nn.setWeight(0, 0, 0, .78);
    nn.setWeight(0, 0, 1, .42);
    nn.setWeight(0, 0, 2, .21);
    nn.setWeight(0, 0, 3, .37);
    nn.setWeight(0, 1, 0, .89);
    nn.setWeight(0, 1, 1, .04);
    nn.setWeight(0, 1, 2, .69);
    nn.setWeight(0, 1, 3, .93);
    nn.setWeight(0, 2, 0, .91);
    nn.setWeight(0, 2, 1, .11);
    nn.setWeight(0, 2, 2, .63);
    nn.setWeight(0, 2, 3, .87);
    nn.setWeight(1, 0, 0, .90);
    nn.setWeight(1, 0, 1, .12);
    nn.setWeight(1, 0, 2, .05);
    nn.setWeight(1, 1, 0, .47);
    nn.setWeight(1, 1, 1, .09);
    nn.setWeight(1, 1, 2, .01);
    
    // Feed forward
    nn.feedForward();
    
    // Read the output
    float output = nn.getOutputNeurons(1);
    std::cout << "Feed forward output: " << output << std::endl;
    
    // Read the topology
    std::cout << "(Number of neurons, number of weights) = (" << nn.numberOfNeurons();
    std::cout << ", " << nn.numberOfWeights() << ")" << std::endl;
    
    return 0;
}
