"""
Construct a neural network with TensorFlow integrating the biases as sum.
"""

# Imports
import tensorflow as tf
import numpy as np
import os

# Read weights
weightsRaw = []
with open("../../weights.dat", "r") as f:
    weightsRaw = f.read().splitlines()
weights = []
indices = []
for iLine, line in enumerate(weightsRaw):
    if len(line)>0 and line[0] == "#":
        indices.append(iLine)
for i in range(len(indices)):
    # Get index
    if i != len(indices)-1:
        iStart = indices[i]
        iEnd   = indices[i+1]
    else:
        iStart = indices[i]
        iEnd   = len(weightsRaw)
    # Substring
    matrixList = weightsRaw[iStart+1:iEnd-1]
    matrixString = "\n".join(matrixList)
    # Write to tmp file
    with open("tmp", "w") as f:
        for line in matrixString:
            f.write(line)
    # Matrix
    w = np.loadtxt("tmp")
    weights.append(w)
    # Delete file
    os.remove("tmp")
    
# Read input
inputs = np.loadtxt("../../in.dat")

# Read output
outputs = np.loadtxt("../../out.dat")

# Derive topology
nrNeurons = []
for w in weights:
    nrNeurons.append(w.shape[1]-1)
nrNeurons.append(weights[-1].shape[0]-1)

# Topology
nrInputNeurons = nrNeurons[0]
nrOutputNeurons = nrNeurons[-1]
nrLayers = len(nrNeurons)


activation = tf.nn.relu # TODO: Check that!


# Partition weights in actual weights and biases
weightsLoaded = []
biasesLoaded  = []
for w in weights:
    bias = w[0, 1:]
    weight = w[1:, 1:]
    weightsLoaded.append(weight)
    biasesLoaded.append(bias)
    
# CHECK DIMENSIONS!

for i in range(len(nrNeurons)):
    if i != len(nrNeurons)-1:
        print(nrNeurons[i])
        print(biasesLoaded[i].shape)
        print(weightsLoaded[i].shape)
        
    print()

# More activation functions: https://www.tensorflow.org/versions/r0.12/api_docs/python/nn/activation_functions_

x = tf.placeholder(tf.float64, shape=[nrInputNeurons, 1])


xTest = np.random.rand(nrInputNeurons, 1)

# =============================================================================
#                            Construction function
# =============================================================================

def buildWeights():
    weights = {}
    for l in range(nrLayers-1):
        #weights[l] = tf.Variable(tf.random_normal([nrNeurons[l+1], nrNeurons[l]],
                                                  #stddev=0.1), name="W{}".format(l))
        weights[l] = tf.Variable(weightsLoaded[l], name="W{}".format(l))
    return weights

def buildBiases():
    biases = {}
    for l in range(nrLayers-1):
        #biases[l] = tf.Variable(tf.random_normal([nrNeurons[l+1],1],
                                                 #stddev=0.1), name="b{}".format(l))
        biases[l] = tf.Variable(biasesLoaded[l], name="b{}".format(l))
    return biases

def buildModel(weights, biases, activation, x, verbose=False):
    n0 = x
    n = [n0, ]
    for l in range(1, nrLayers):
        if verbose:
            print(weights[l-1])
            print(n[l-1])
            print(biases[l-1])
        nl = activation(
                tf.add(tf.matmul(weights[l-1], n[l-1]), biases[l-1]), name="n{}".format(l)
             )
        if verbose:
            print(nl)
            print()
        n.append(nl)
    return n[-1]

# =============================================================================
#                                 Construction
# =============================================================================
    
weights = buildWeights()
biases  = buildBiases()

print("jooo")
model = buildModel(weights, biases, activation, x)

# =============================================================================
#                                  Evaluation
# =============================================================================

# Initialize the variables
init = tf.global_variables_initializer()

# For saving
saver = tf.train.Saver()

# Start training
with tf.Session() as sess:

    # Init the variables (weights and biases)
    sess.run(init)
    
    # Get weight and bias values
    print(sess.run(weights[0]))

    # Feedforward
    output = sess.run(model, feed_dict={x: xTest})
    
    
# TODO: Save the input, output and weights (weights+biases) in format that
# my C++ code understands.
