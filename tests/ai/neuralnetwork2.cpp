#include<iostream>
#include "../../include/neuralnetwork.h"
#include<vector>
#include <random>
#include <fstream>

int main(void) {
    
    // Construct neural network
    std::vector<std::size_t> nrNeurons{7,10,4};
    AI::NeuralNetwork nn(nrNeurons, AI::NeuralNetwork::ACTIVATION_LINEAR, true, true);
    std::size_t nrInput = nrNeurons.front();
    std::size_t nrOutput = nrNeurons.back();
    
    // Random generator for weights
    int seed = 1;
    std::default_random_engine re(seed);
    std::normal_distribution<double> dist(0, 0.5);
    // TODO: Does the seed work? Last time I tested it, it didn't!
    
    // Random weights
    for (std::size_t l=0; l<nrNeurons.size()-1; l++) {
        for (std::size_t endNeuron=0; endNeuron<nrNeurons[l+1]+1; endNeuron++) {
            for (std::size_t startNeuron=0; startNeuron<nrNeurons[l]+1; startNeuron++) {
                nn.setWeight(l, endNeuron, startNeuron, dist(re));
            }
        }
    }
    
    // Save weights
    nn.saveWeights("weights.dat");
    
    // Random generator for input
    std::default_random_engine re2(seed);
    std::uniform_real_distribution<double> dist2(-5, +5);
    // TODO: Does the seed work? Last time I tested it, it didn't!
    
    // Random input values
    std::size_t nrSamples = 10;
    
    // Data to store
    std::ofstream fIn;
    std::ofstream fOut;
    fIn.open("in.dat");
    fOut.open("out.dat");
    
    for (std::size_t ctr=0; ctr<nrSamples; ctr++) {
    
        // Set input
        for (std::size_t i=1; i<nrInput+1; i++) {
            double v = dist2(re2);
            nn.setInputNeurons(i, v);
            fIn << v << " ";
        }
        fIn << "\n";
        
        // Feed forward
        nn.feedForward();
        
        // Get output
        for (std::size_t i=1; i<nrOutput+1; i++) {
            fOut << nn.getOutputNeurons(i) << " ";
        }
        fOut << "\n";
    
    }
    
    fIn.close();
    fOut.close();
    
    return 0;
}
