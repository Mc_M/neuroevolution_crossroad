#include <iostream>
#include <Eigen/Dense>
#include <vector>
#include <algorithm>
#include <memory>

using Eigen::MatrixXd;
int main()
{
  MatrixXd m(2,2);
  m(0,0) = 3;
  m(1,0) = 2.5;
  m(0,1) = -1;
  m(1,1) = m(1,0) + m(0,1);
  std::cout << m << std::endl;
  
  MatrixXd v(2, 1);
  v(0,0) = 1;
  v(1,0) = 2;
  std::cout << v << std::endl;
  
  MatrixXd mult(2,1);
  mult = m*v;
  std::cout << mult << std::endl;
  
  MatrixXd a = m*v;
  
  std::vector< std::unique_ptr< MatrixXd > > weights_;
  
  weights_.push_back(std::make_unique< MatrixXd >(2, 2));
  
  std::cout << (*(weights_[0]))(1,1) << std::endl;
  
}
