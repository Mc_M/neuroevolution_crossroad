import numpy as np
import sys

print("USAGE: {lin, tanh, relu, sigmoid}")

actLin = lambda x: x
actTanh = lambda x: np.tanh(x)
def actRelu(x):
    x2 = x.copy()
    x2[x2<0] = 0.0
    return x2
actSigmoid = lambda x: 1/(1+np.exp(-x))

act = sys.argv[1]
if act=="lin":
    act = actLin
elif act=="tanh":
    act = actTanh
elif act=="relu":
    act = actRelu
elif act=="sigmoid":
    act = actSigmoid

w0 = np.array([[.78, .42, .21, .37],
               [.89, .04, .69, .93],
               [.91, .11, .63, .87]])

w1 = np.array([[.90, .12, .05],
               [.47, .09, .01]])

n0 = np.array([[1],
               [3],
               [2],
               [4]])

n1 = np.dot(w0, n0)
n1 = act(n1)
n1[0] = 1.0

n2 = np.dot(w1, n1)
n2 = act(n2)
n2[0] = 1.0

print(n2)
