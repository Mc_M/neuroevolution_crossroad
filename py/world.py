import matplotlib
import numpy as np

class World(object):
    """
    Representation of rectengular cross road world.
    """
    
    counter = 0
    
    Null_World = None # Doesn't support real World's attributes

    def __init__(self, Lx=10.0, Ly=8.0, t0=0.0, dt=0.1):
        """
        Construtor.

        :param: t0 : Initial time.
        :param: Lx : Horizontal length of this world.
        :param: Ly : Vertical length of this world.
        :param: dt : Time step for advancing the world.
        """
        self.t0                 = t0
        self.Lx                 = Lx
        self.Ly                 = Ly
        self.dt                 = dt
        self.tsteps_            = 0
        # Private variables that can change
        self.t_                 = t0
        self.autobahns_         = []
        self.tsteps_            = 0
        # Update World counter
        World.counter           += 1

    def autobahns(self):
        """
        Return list of the autobahn objects of this world.
        """
        return self.autobahns_

    def t(self):
        """
        Return the current world time.
        """
        return self.t_
        
    def tsteps(self):
        """
        Return the number of time steps taken in this world.
        """
        return self.tsteps_

    def advance(self):
        """
        Advance the world one time step.
        """
        # Update time
        self.t_                         += self.dt
        self.tsteps_                    += 1
        # Update all cars on all autobahns
        for a in self.autobahns():
            for c in a.cars():
                c.advance(self.t())

    def draw(self, outdir):
        """
        Draw the current state of the world to a picture.
        """
        # Create figure
        from matplotlib import pyplot as plt
        from matplotlib import patches
        fig = plt.figure()
        ax = fig.add_subplot(111, aspect='equal')
        ax.set_xlim(0.0, self.Lx)
        ax.set_ylim(0.0, self.Ly)
        ax.set_title("t = {:5.3f}".format(self.t()))
        # Draw agents
        for a in self.autobahns():
            # Draw the autobahn itself
            ax.add_patch(
                    patches.Rectangle(
                            (0, a.pos-a.w/2.0),
                            self.Lx,
                            a.w,
                            facecolor="lightgray",
                        )
                    )
            # Draw its cars
            for car in a.cars():
                pos_of_car = car.pos()
                ax.add_patch(
                        patches.Rectangle(
                             (pos_of_car[0]-car.l/2, pos_of_car[1]-car.w/2),
                             car.l,
                             car.w,
                             facecolor=car.color
                        )
                    )

        fig.savefig(outdir, dpi=90, bbox_inches='tight')
        plt.close(fig) # Saves memory!

class Autobahn(object):
    
    counter = 0
    
    Null_Autobahn = None # Doesn't support real Autobahn's attributes
    
    def __init__(self, world,    # The world this autobahn was built in.
                       vx=1.0,
                       w=1.0,
                       pos=1.0):
        # Input
        self.vx                 = vx
        self.vy                 = 0.0
        self.w                  = w # TODO: Check that not too large!
        self.pos                = pos # TODO: Check that in world geometry.
        self.world              = world
        # Derived
        self.extend =  (
                          self.pos-self.w/2.0,
                          self.pos+self.w/2.0
                        ) # TODO: Check that in world geometry!
        # Private attributes that can change
        self.cars_              = []
        # Update Autobahn counter
        Autobahn.counter        += 1
    
    def cars(self):
        """
        Return list of cars on this autobahn.
        """
        return self.cars_
    
class Car(object):
    """
    Represents a car.
    
    It is placed on an Autobahn and has a state.
    """
    
    counter = 0
    
    def __init__(self, autobahn, # The autobahn this car drives on.
                       vx=1.0,
                       vy=1.0,
                       color="black",
                       l=1.0,
                       w=1.0,
                       x0=0.0,
                       y0=0.0,
                       bc="none"):
        self.autobahn       = autobahn
        self.vx             = vx
        self.vy             = vy
        self.color          = color
        self.l              = l
        self.w              = w
        self.x0             = x0
        self.y0             = y0
        self.bc             = bc
        # Private variables that can change
        self.x_             = x0
        self.y_             = y0
        # Update car counter
        Car.counter         +=1
    
    def advance(self, t):
        """
        Advance the car.
        
        The position is update to the trajectory.
        """
        if self.bc == "periodic":
            # Compute potential new position
            x, y = self.trajectory(t)
            # Project back into world
            Lx = self.autobahn.world.Lx
            Ly = self.autobahn.world.Ly
            while not 0 <= x <= Lx:         # x
                if x > Lx:
                    x -= Lx
                else:
                    x += Lx
            while not 0 <= y <= Ly:         # y
                if y > Ly:
                    y -= Ly
                else:
                    y += Ly
            self.x_, self.y_ = x, y
                
        elif self.bc == "none":
            self.x_, self.y_ = self.trajectory(t)
            
        # TODO: The extend should be devided to left and right!
    
    def pos(self):
        """
        Return car's current position.
        """
        return (self.x_, self.y_)
        
    def extend(self):
        """
        Return edge points of the car.
        
        Order: Top left (tl), bottom left (bl), bottom right (br), top right (tr).
        """
        tl = (self.x_-self.l/2, self.y_+self.w/2)
        bl = (self.x_-self.l/2, self.y_-self.w/2)
        tr = (self.x_+self.l/2, self.y_+self.w/2)
        br = (self.x_+self.l/2, self.y_-self.w/2)
        return (tl, bl, br, tr)
        
    def trajectory(self, t):
        """
        Return point in space at time t according to car's equation of motion.
        """
        return (
                    self.x0 + self.vx * t,
                    self.y0 + self.vy * t
                )

if __name__ == "__main__":
    
    import random
    
    # Test cars
    print("Test Car()")
    c = Car(Autobahn.Null_Autobahn, x0=0, y0=1.0, vx=1.0, vy=0.0, l=2.0, w=0.8)
    print(c.pos())
    print(c.extend())
    print(c.trajectory(0.0))
    print(c.advance(0.0))
    print(c.pos())
    print(Car.counter)
    print()
    
    # Test autobahns
    print("Test Autobahn():")
    a = Autobahn(World.Null_World)
    print(a.counter)
    print(a.pos)
    print(a.w)
    print(a.extend)
    print(a.vx)
    print(a.vy)
    print()
    
    # Test worlds
    print("Test World():")
    Lx = 10.0                                           # World settings
    Ly = 8.0
    t0 = 0.0
    dt = 0.5
    car_colors = [
                      "blue",
                      "red",
                      "plum",
                      "gold",
                      "green",
                      "cyan",
                      "indigo",
                  ]
    world = World(Lx, Ly, t0, dt)                       # Create world
    m = int(Ly)-1                                       # Add autobahns
    for i in range(m):
        world.autobahns().append(
            Autobahn(world, w=0.8, pos=float(i)+1.0)
        )
    for i in range(m):                                  # Add cars
        a = world.autobahns()[i]
        for j in range(int(Lx)//2-1):
            a.cars().append(
                Car(a, l=1.5, w=0.6, x0=j*2.0+2, y0=a.pos, vy=0.0, vx=(-1)**i,
                    color=random.choice(car_colors), bc="periodic")
            )
        # TODO: Fix that cars still have indipendent velocity! Do that
        # by introducing an uplink to world and autobahn.
        # TODO: Implement world's periodic boundary conditions
    # Draw the world
    import os
    os.makedirs("./pics")
    for i in range(50):
        print(i)
        world.draw("./pics/test.{:03}.png".format(i))   # Draw world
        world.advance()                         # Advance world