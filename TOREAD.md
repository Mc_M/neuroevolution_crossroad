https://www.reddit.com/r/MachineLearning/comments/6ed5pv/d_on_the_influence_of_zeroinputs_in_neural/



https://www.google.de/search?client=ubuntu&channel=fs&q=neural+network+zero+input&ie=utf-8&oe=utf-8&gfe_rd=cr&dcr=0&ei=i8lCWtnhLo-O8Qf7h6dY



* GA benchmark: https://www.analyticsvidhya.com/blog/2017/07/introduction-to-genetic-algorithm/, travels and sales man
  problem in edinburgh w/ real map data, https://www.codeproject.com/Articles/16286/AI-Simple-Genetic-Algorithm-GA-to-solve-a-card-pro



Application: https://www.neuraldesigner.com/blog/genetic_algorithms_for_feature_selection, 


GA:
* "genetic algorithm floating point encoding"
* "read about ga"
* http://www.sciencedirect.com/science/article/pii/S0045782599003898
* http://www.cs.umsl.edu/~janikow/publications/1991/GAbin/text.pdf
* ! http://www1.icsi.berkeley.edu/~storn/code.html
* http://www.amichel.com/de/doc/html/
* https://en.wikipedia.org/wiki/Tournament_selection
* https://stackoverflow.com/questions/14622342/elitism-in-ga-should-i-let-the-elites-be-selected-as-parents
* http://www.omgwiki.org/hpec/files/hpec-challenge/ga.html
* http://www.theprojectspot.com/tutorial-post/creating-a-genetic-algorithm-for-beginners/3
* https://blog.sicara.com/getting-started-genetic-algorithms-python-tutorial-81ffa1dd72f9
* https://uk.mathworks.com/help/gads/examples/constrained-minimization-using-the-genetic-algorithm.html


Cool example: https://arztsamuel.github.io/en/projects/unity/deepCars/deepCars.html


Uniform crossover: random exchange

Idea on mutation:
x) Recast any type to bool/byte string -> "fixed-point integer encoding". Issue is hamming cliff and solution
   is Gray coding (delta hamming = 1).
x) 


GAs:
x) Convert float to bit and use normal genetic operations: https://stackoverflow.com/questions/8059775/convert-between-double-and-byte-array-for-transfer-over-zigbee-api.
x) Details on specific genetic operators: https://en.wikipedia.org/wiki/Genetic_algorithm
x) Mutation:
    x) https://en.wikipedia.org/wiki/Mutation_(genetic_algorithm)
    x) "genetic algorithm mutation operator"
    x) http://ijcsit.com/docs/Volume%205/vol5issue03/ijcsit20140503404.pdf
x) Very good tutorial on all aspects of GAs: https://www.tutorialspoint.com/genetic_algorithms/genetic_algorithms_crossover.htm
