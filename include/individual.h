// Individual
//
// Classes implementing this interface can be treated as individual that
// are used in Genetic Algorithms.

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <vector>
#include <iostream>
#include <string>

namespace Genetics {

	template <class GeneType>
	class Individual {
		
		public:
			
			// Constructors
			//~ virtual Individual() // TODO: COPY CONSTRUCTOR MUST BE IMPLEMENTED
			virtual ~Individual() {
				// TODO: Print if debug flag is on.
				//~ std::cout << "Destructor of Individual called!" << std::endl;
			};
			
			// Function memebers
			virtual double fitness() = 0; // 0 <= fitness; should be cached because it will be called often.
			virtual std::size_t genomeLength() = 0;
			virtual void setGene(int index, GeneType gene) = 0;
			virtual GeneType getGene(int index) = 0;
			virtual std::string print() = 0;
			
			//
			// Comment: Any fitness caching must be managed by the implementation
			// of the individual. Algorithms that use Individuals use the individual's
			// fitness function liberately so that caching the value from expensive
			// fitness evaluations is recommended.
			//
			
			// Derived functions
			std::vector<GeneType> getGenome() {
				// Wasteful! Should be avoided.
				std::vector<GeneType> genome;
				genome.resize(genomeLength());
				for (std::size_t ii=0; ii<genomeLength(); ii++) {
					genome[ii] = getGene(ii);
				}
				return genome;
			}
			
			// Return 0 if the genome was set. Return 1 otherwise
			int setGenome(std::vector<GeneType> &genome) {
				if (genome.size() == genomeLength()) {
					for (std::size_t ii=0; ii<genomeLength(); ii++) {
						setGene(ii, genome[ii]);
					}
					return 0;
				} else {
					return 1;
				}
			}
			
	};

}

#endif
