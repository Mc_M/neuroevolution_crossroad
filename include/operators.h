// Operators
//
// Classes that implement this interface act as genetic operator upon
// classes implementing the Individual interface.
//
// Additional settings for the genetic operators can be used from the
// implementing class.

#include "individual.h"

namespace Genetics {

	template <class GeneType>
	class Operators {
		
		public:
			
			// Constructors
			virtual ~Operators() {};
			
			// Function memebers
			virtual void mutate(Individual<GeneType> individual) = 0;
			virtual Individual<GeneType> crossover(Individual<GeneType> individual1,
												   Individual<GeneType> individual2) = 0;

	};

}
