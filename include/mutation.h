// Mutation
//
// Static class that provides pre-implemented mutation operators.

#ifndef MUTATION_H
#define MUTATION_H

#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>

namespace Genetics {

    class Mutation {

        public:

            // Constructors
            Mutation() {};
            ~Mutation() {};

            // Function members
            static void intGaussian(int &value,
                                    GeneticAlgorithmSettings &settings);
            
            // Other possible operator implementations:
            // x) Bitflip
            // x) Normal distribution around old value

    };

}

// ========================= Implementations ===========================

void Genetics::Mutation::intGaussian(int &value, GeneticAlgorithmSettings &settings) {
    // A rounded and casted normal distribution is used. A binomial distribution
    // could be used according to "De Moivre–Laplace theorem" but then the width
    // would be hard to control.
    std::normal_distribution<float> dist(value, settings.width);
    value = round(dist(settings.randomGenerator));
}

#endif
