#ifndef GENETICSALGORITHMSETTINGS_H
#define GENETICSALGORITHMSETTINGS_H

namespace Genetics {

    struct GeneticAlgorithmSettings {
        // Settings
        std::size_t tournamentSize = 0;
        std::size_t numberOfGenerations = 0;
        std::size_t nElite = 0;
        std::size_t seed = 0;
        std::size_t nPoint = 0;
        double mutationProbability = 0.0;
        double width = 0.0;
        // Objects
        std::default_random_engine randomGenerator;
    } ;

}

#endif
