// Population
//
// Class that represents a population of Individual objects.

#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include "individual.h"
#include <vector>
#include <algorithm>
#include <cstddef>
#include <functional>

// ------------------------- Declarations ------------------------------

namespace Genetics {

    template <class GeneType>
    class GeneticAlgorithm {

        public:
            
            // Constructors
            GeneticAlgorithm(std::shared_ptr< Population< GeneType > > popPtr,
                             GeneticAlgorithmSettings settings,
                             std::function<void(std::shared_ptr< Population<GeneType> >,
                                                std::size_t,
                                                std::vector<std::size_t> &,
                                                Genetics::GeneticAlgorithmSettings &)> selectionFunction,
                             std::function<void(std::size_t,
                                                std::vector<GeneType> &,
                                                std::vector<GeneType> &,
                                                Genetics::GeneticAlgorithmSettings &)> crossoverFunction,
                             std::function<void(GeneType &,
                                                GeneticAlgorithmSettings &)> mutationFunction);
            ~GeneticAlgorithm() {  };

            // Function members
//             void run();             // Compute all generations
            void advance();          // Compute next generation
        

        public:

            std::shared_ptr< Population< GeneType > > popPtr_;
            std::size_t numberOfGenerations_;
            std::size_t nElite_;
            std::vector<std::size_t> parentIndices_;
            std::vector< GeneType > genomeBuffer_;
            std::vector< GeneType > genomeBufferElite_;
            std::vector< std::size_t > eliteInjectionIndices_;
            std::vector< GeneType > genome1_;
            std::vector< GeneType > genome2_;
            std::size_t nrIndividuals_;
            std::size_t genomeLength_;
            std::pair< std::size_t, std::size_t > indexPair_;
            std::function<void(std::shared_ptr< Population<GeneType> >,
                               std::size_t,
                               std::vector<std::size_t> &,
                               Genetics::GeneticAlgorithmSettings &)> selectionFunction_;
            std::function<void(std::size_t,
                               std::vector<GeneType> &,
                               std::vector<GeneType> &,
                               Genetics::GeneticAlgorithmSettings &)> crossoverFunction_;
            std::function<void(GeneType &,
                               GeneticAlgorithmSettings &)> mutationFunction_;
            GeneticAlgorithmSettings settings_;
            std::uniform_real_distribution<> uniformDistribution_;
            
            // Format: (indexIndividual, indexGene)
            std::size_t to1D(std::size_t indexIndividual, std::size_t indexGene) {
                return indexGene+indexIndividual*genomeLength_;
            }
            
            std::pair<std::size_t, std::size_t> to2D(std::size_t index) {
                std::size_t indexGene = index % genomeLength_;
                std::size_t indexIndividual = index / genomeLength_;
                return std::make_pair(indexIndividual, indexGene);
            }

    };

}

// -------------------------- Definitions ------------------------------

template <class GeneType>
Genetics::GeneticAlgorithm<GeneType>::GeneticAlgorithm(                                                      
                             std::shared_ptr< Population< GeneType > > popPtr,
                             GeneticAlgorithmSettings settings,
                             std::function<void(std::shared_ptr< Population<GeneType> >,
                                                std::size_t,
                                                std::vector<std::size_t> &,
                                                Genetics::GeneticAlgorithmSettings &)> selectionFunction,
                             std::function<void(std::size_t,
                                                std::vector<GeneType> &,
                                                std::vector<GeneType> &,
                                                Genetics::GeneticAlgorithmSettings &)> crossoverFunction,
                             std::function<void(GeneType &,
                                                GeneticAlgorithmSettings &)> mutationFunction) {
    
    
    popPtr_ = popPtr;
    numberOfGenerations_ = settings.numberOfGenerations;
    nElite_ = settings.nElite;
    eliteInjectionIndices_.resize(nElite_);
    selectionFunction_ = selectionFunction;
    crossoverFunction_ = crossoverFunction;
    mutationFunction_ = mutationFunction;
    settings_ = settings;
    nrIndividuals_ = popPtr_->size();
    assert(nrIndividuals_%2==0 && "Population size must be even.");
    if (nrIndividuals_ > 0) {
        genomeLength_ = popPtr_->getIndividual(0)->genomeLength();
    } else {
        genomeLength_ = -1;
    }
    parentIndices_.resize(nrIndividuals_);
    genomeBuffer_.resize(nrIndividuals_*genomeLength_);
    genomeBufferElite_.resize(nElite_*genomeLength_);
    genome1_.resize(nrIndividuals_);
    genome2_.resize(nrIndividuals_);
    uniformDistribution_ = std::uniform_real_distribution<>(0.0, 1.0);
}

template <class GeneType>
void Genetics::GeneticAlgorithm<GeneType>::advance() {
    // Compute fitness
    for (std::size_t i=0; i<popPtr_->size(); i++) {
        popPtr_->getIndividual(i)->fitness();
    }
    // Determine mutation probability
// TODO based on generation number, max number and fitness values
    // Save genome of elite individuals
    popPtr_->updateRanking();
    for (std::size_t indexIndividual=0; indexIndividual<nElite_; indexIndividual++) {
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++) {
            genomeBufferElite_[to1D(indexIndividual, indexGene)] = popPtr_->getEliteIndividualGene(indexIndividual, indexGene);
        }
    }
    // Select parents
    selectionFunction_(popPtr_, nrIndividuals_, parentIndices_, settings_);
    // Copy genomes from parents
    for (std::size_t indexIndividual : parentIndices_){
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++){
            genomeBuffer_[to1D(indexIndividual, indexGene)] = popPtr_->getIndividual(indexIndividual)->getGene(indexGene);
        }
    }
    // Cross over
    for (std::size_t i=0; i<nrIndividuals_/2; i++) {
        std::size_t i1 = 2*i;
        std::size_t i2 = 2*i+1;
        // Copy data
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++) {
            genome1_[indexGene] = genomeBuffer_[to1D(i1, indexGene)];
            genome2_[indexGene] = genomeBuffer_[to1D(i2, indexGene)];
        }
        // Cross over
        crossoverFunction_(genomeLength_, genome1_, genome2_, settings_);
        // Copy data back
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++) {
            genomeBuffer_[to1D(i1, indexGene)] = genome1_[indexGene];
            genomeBuffer_[to1D(i2, indexGene)] = genome2_[indexGene];
        }
    }
    // Mutation
    for (std::size_t indexIndividual=0; indexIndividual<nrIndividuals_; indexIndividual++) {
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++) {
            if (uniformDistribution_(settings_.randomGenerator) < settings_.mutationProbability) {
                // Mutate a single gene
                mutationFunction_(genomeBuffer_[to1D(indexIndividual, indexGene)], settings_);
            }
        }
    }
    // Inject elite individuals at random (and different) indices
    bool found;
    std::size_t val;
    for (std::size_t i=0; i<nElite_; i++) {
        found = false;
        do {
            val = settings_.randomGenerator() % nrIndividuals_;
            found = false;
            for (std::size_t j=0; j<i; j++) {
                if (eliteInjectionIndices_[j]==val) {
                    found = true;
                    break;
                }
            }
        } while (found);
        eliteInjectionIndices_[i] = val;
    }
    for (std::size_t i=0; i<nElite_; i++) {
        std::size_t indexIndividual = eliteInjectionIndices_[i];
        for (std::size_t indexGene; indexGene<genomeLength_; indexGene++) {
            genomeBuffer_[to1D(indexIndividual, indexGene)] = genomeBufferElite_[to1D(i, indexGene)];
        }
    }
    // Copy genes back to population
    for (std::size_t indexIndividual=0; indexIndividual<nrIndividuals_; indexIndividual++){
        for (std::size_t indexGene=0; indexGene<genomeLength_; indexGene++){
            GeneType gene = genomeBuffer_[to1D(indexIndividual, indexGene)];
            popPtr_->getIndividual(indexIndividual)->setGene(indexGene, gene);
        }
    }
    
    // TODO: Maybe update mutation probability?? Could depend on time, fitness statistics.
    //       But also one could decrease the mutation strength (magnitude, relativ instead
    //       of random, ...)
    
    // Increase generations counter
    // TODO
}

#endif
