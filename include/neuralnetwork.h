#include <vector>
#include <memory>
#include <Eigen/Dense>
#include <functional>

namespace AI {

class NeuralNetwork {
	
    public:
        
        // Activation functions
        static double activationLinear(double in);
        static double activationSigmoid(double in);
        static double activationTanh(double in);
        static double activationRelu(double in);
            
        // Constants
        static const int ACTIVATION_LINEAR  = 0;
        static const int ACTIVATION_SIGMOID = 1;
        static const int ACTIVATION_TANH    = 2;
        static const int ACTIVATION_RELU    = 3;
        
        // Constructors
        NeuralNetwork(std::vector<std::size_t> nrNeurons, int activationFunctionMode,
                      bool verbose, bool debug);
        NeuralNetwork();
        
        // Neurons
        void setInputNeurons(std::size_t index, double value);
        double getOutputNeurons(std::size_t index);
        double getNeuronValue(std::size_t layer, std::size_t number);
        
        // Weights
        void setWeight(std::size_t layer, std::size_t endNeuron, std::size_t startNeuron, double value);
        double getWeight(std::size_t layer, std::size_t endNeuron, std::size_t startNeuron);
        void saveWeights(const std::string& path);

        // Evaluation
        void feedForward();
        
        // Computation of degrees of freedom
        int numberOfNeurons();
        int numberOfWeights();

    private:
    
        bool verbose_;
        bool debug_;
        std::size_t nrLayers_;
        std::vector<std::size_t> nrNeurons_;
        std::vector< std::unique_ptr< Eigen::MatrixXd > > weights_;
        std::vector< std::unique_ptr< Eigen::VectorXd > > neurons_;
        std::function<double(double)> activationFunction_;
                
};

}
