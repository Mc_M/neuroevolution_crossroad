// Population
//
// Class that represents a population of Individual objects.

#ifndef POPULATION_H
#define POPULATION_H

#include "individual.h"
#include <vector>
#include <algorithm>
#include <cstddef>
#include <cassert>

// ------------------------- Declarations ------------------------------

namespace Genetics {

	template <class GeneType>
	class Population {

		public:

			// Constructors
			Population(std::size_t capacity) {
				capacity_ = capacity;
				size_ = 0;
				individuals_.resize(capacity_);
				std::cout << "hi" << std::endl;
				std::cout << individuals_.size() << "  " << individuals_.capacity() << std::endl;
			}
			~Population() {};

			// Function members
			std::size_t size();
			std::size_t capacity();
			float highestFitness();
			float averageFitness();
			float lowestFitness();
			void addIndividual(std::shared_ptr< Individual<GeneType> > individual);
			std::shared_ptr< Individual<GeneType> > getIndividual(std::size_t index);
			//~ std::shared_ptr< Individual<GeneType> > getFittestIndividual();
                        void updateRanking();
                        GeneType getEliteIndividualGene(std::size_t indexEliteIndividual, std::size_t indexGenome);
                        std::shared_ptr< Individual<GeneType> > getFittestIndividual(std::size_t indexIndividual);

		public:

			std::size_t capacity_;
			std::size_t size_;
			std::vector< std::shared_ptr< Individual<GeneType> > > individuals_;
			std::vector< float > tmpFitnesses_;
                        std::vector< std::pair< double, std::size_t > > ranking_;

	};

}

// -------------------------- Definitions ------------------------------

template <class GeneType>
Genetics::Population<GeneType>::Population(std::size_t capacity) {
        assert(capacity%2==0 && "Population size must be even.");
	capacity_ = capacity;
	size_ = 0;
	individuals_.resize(capacity_);
	tmpFitnesses_.resize(capacity_);
        ranking_.resize(capacity_);
}

template <class GeneType>
std::size_t Genetics::Population<GeneType>::size() {
	return size_;
}

template <class GeneType>
std::size_t Genetics::Population<GeneType>::capacity() {
	return capacity_;
}

template <class GeneType>
float Genetics::Population<GeneType>::highestFitness() {
	float fMax = 0.0;
	for(std::size_t ii=0; ii<size(); ii++) {
		float f = individuals_[ii]->fitness();
		if (f > fMax) {
			fMax = f;
		}
	}
	return fMax;
}

template <class GeneType>
float Genetics::Population<GeneType>::averageFitness() {
	float f = 0.0;
	for(std::size_t ii=0; ii<size(); ii++) {
		f += individuals_[ii]->fitness();
	}
	if (size() != 0) {
		return f/size();
	} else {
		return 0.0;
	}
}

template <class GeneType>
float Genetics::Population<GeneType>::lowestFitness() {
	std::size_t s = size();
	float fMin = -1.0;
	for(std::size_t ii=0; ii<s; ii++) {
		if (ii==0) {
			fMin = individuals_[ii]->fitness();
		} else {
			float f = individuals_[ii]->fitness();
			if (f < fMin) {
				fMin = f;
			}
		}
	}
	return fMin;
}

//
// Only added if size < capacity - TODO: Throw exception!
//
template <class GeneType>
void Genetics::Population<GeneType>::addIndividual(std::shared_ptr< Individual<GeneType> > individual) {
	if (size() < capacity()) {
		individuals_[size()] = individual;
		size_ += 1;
	}
}

//
// Only return an individual with valid index - TODO: Throw exception
//
// index is zero-indexed
//
template <class GeneType>
std::shared_ptr< Genetics::Individual<GeneType> > Genetics::Population<GeneType>::getIndividual(std::size_t index) {
    assert(index>=0 and index<size() && "Individual index not valid");
    return individuals_[index];
}

template <class GeneType>
std::shared_ptr< Genetics::Individual<GeneType> > Genetics::Population<GeneType>::getFittestIndividual(std::size_t indexIndividual) {
    return getIndividual(ranking_[indexIndividual].second);
}

template <class GeneType>
void Genetics::Population<GeneType>::updateRanking() {
    for (std::size_t i=0; i<size(); i++) {
        ranking_[i].first = getIndividual(i)->fitness();
        ranking_[i].second = i;
    }
    // Sorted starting from the fittest
    std::sort(ranking_.begin(), ranking_.begin()+size(), std::greater< std::pair< double, std::size_t > >());
}

//
// indexEliteIndividual==0 is the fittest individual
//
// updateRanking() should have been called previously.
//
template <class GeneType>
GeneType Genetics::Population<GeneType>::getEliteIndividualGene(std::size_t indexEliteIndividual, std::size_t indexGenome) {
    assert(indexEliteIndividual>=0 and indexEliteIndividual<size() && "indexEliteIndividual must be a valid indidual index.");
    assert(indexGenome>=0 and indexGenome<getIndividual(0)->genomeLength() && "indexGenome must be a valid genome index.");
    std::size_t individualIndex = ranking_[indexEliteIndividual].second;
    return getIndividual(individualIndex)->getGene(indexGenome);
}

#endif
