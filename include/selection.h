// Selection
//
// Static class that provides pre-implemented selection algorithms.

#include "individual.h"
#include "population.h"
#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>

namespace Genetics {

    class Selection {

        public:

            // Constructors
            Selection() {};
            ~Selection() {};

            // Function members
            template <class GeneType>
            static void tournamentSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                            std::size_t numberOfParents,
                                            std::vector<std::size_t> &parentIndices,
                                            GeneticAlgorithmSettings &settings);

    };

}

// ========================= Implementations ===========================

template <class GeneType>
void Genetics::Selection::tournamentSelection(std::shared_ptr< Population<GeneType> > popPtr,
                                              std::size_t numberOfParents,
                                              std::vector<std::size_t> &parentIndices,
                                              GeneticAlgorithmSettings &settings) {	
	assert(numberOfParents == parentIndices.size() && "parentIndices is not big enough");
	for (std::size_t ii=0; ii<numberOfParents; ii++) {
		int chosenIndex = -1;
		for (std::size_t jj=0; jj<settings.tournamentSize; jj++) {
			int randomIndividualIndex = settings.randomGenerator() % popPtr->size();
			if (jj == 0) {
				chosenIndex = randomIndividualIndex;
			} else {
				if (   popPtr->getIndividual(randomIndividualIndex)->fitness()
				     > popPtr->getIndividual(      chosenIndex    )->fitness()) {
					chosenIndex = randomIndividualIndex;
				}
			}
		}
		parentIndices[ii] = chosenIndex;
	}
}
