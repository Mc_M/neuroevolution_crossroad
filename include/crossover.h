// Crossover
//
// Static class that provides pre-implemented crossover operators.


#ifndef CROSSOVER_H
#define CROSSOVER_H

#include "geneticalgorithmsettings.h"
#include <vector>
#include <cstddef>
#include <random>
#include <cassert>
#include <algorithm>
#include <iostream>

namespace Genetics {

    class Crossover {

        public:

            // Constructors
            Crossover() {};
            ~Crossover() {};

            // Function members
            template <class GeneType>
            static void npoint(std::size_t genomeLength,
                               std::vector<GeneType> &genome1,
                               std::vector<GeneType> &genome2,
                               GeneticAlgorithmSettings &settings);

    };

}

// ========================= Implementations ===========================

template <class GeneType>
void Genetics::Crossover::npoint(std::size_t genomeLength,
                                 std::vector<GeneType> &genome1,
                                 std::vector<GeneType> &genome2,
                                 GeneticAlgorithmSettings &settings) {
    assert(settings.nPoint<=genomeLength-1 && "settings.nPoint must be <= genomeLength-1");
    std::size_t nPoint = settings.nPoint;
    // Create random intervals to swap in increasing order
    std::vector< std::size_t > boundaries;
    std::size_t val = 0;
    for (std::size_t i=0; i<nPoint; i++) {
        bool found = false;
        do {
            val = settings.randomGenerator() % (genomeLength-1)+1;  // between 1 and genomeLength-1
            found = false;
            for (std::size_t j=0; j<i; j++) {
                if (boundaries[j]==val) {
                    found = true;
                    break;
                }
            }
        } while (found);
        
        boundaries.push_back(val);
    }
    boundaries.push_back(genomeLength);
    boundaries.push_back(0);
    std::sort(boundaries.begin(), boundaries.end());
    // Swap sub genomes (Alternating between swapping and not swapping sub genomes
    // to avoid a complete swap)
    for (std::size_t i=0; i<boundaries.size()-1; i++) {
        if (i%2==0) {
            std::size_t iStart = boundaries[i];
            std::size_t iEnd = boundaries[i+1];
            for (std::size_t j=iStart; j<iEnd; j++) {
                std::swap(genome1[j], genome2[j]);
            }
        }
    }
}

#endif

