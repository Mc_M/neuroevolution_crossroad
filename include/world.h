// World
//
// Implementation of simplified Frogger hooked up to a neural network
// controlled player.

#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <random>
#include <string>
#include <unordered_map>
#include <tuple>
#include "neuralnetwork.h"

namespace Frogger {

    class World {

        public:

            // Constructors
            World(int nx, int ny,
                  int sx, int sy,
                  int tMax,
                  double carDensity,
                  std::vector< std::size_t > topology,
                  int activation,
                  int seed);

            // Constants
            static const int CAR_EMPTY     =  0;
            static const int CAR_FULL      =  1;
            static const int CAR_LEFT      = -1;
            static const int CAR_RIGHT     = +1;
            static const int PLAYER_SYMBOL =  2;
            static const int PLAYER_UP     =  0;
            static const int PLAYER_DOWN   =  1;
            static const int PLAYER_LEFT   =  2;
            static const int PLAYER_RIGHT  =  3;
            static const int PLAYER_STAY   =  4;
            
            // =========
            // Functions
            // =========
            
            // Save the world to file
            void renderData(const std::string& path, const std::string& suffix);
            
            // Advance the cars of the world
            void advanceCars();
            
            // Advance the player of the world
            void advancePlayer();
            
            // Advance the world
            void advance();
            
            // Player hit by car
            bool isPlayerAlive();
            
            // Individual interface methods
            std::size_t genomeLength();
            void setGene(int index, double gene);
            double getGene(int index);
            
            // TODO: int-> std::size_t!

        private:

            // ====
            // Data
            // ====
    
            // World dimension
            int nx_;
            int ny_;
            int area_;

            // Field of sight for player
            int sx_;
            int sy_;
    
            // Time
            int t_;
            int tMax_;

            // Cars
            double carDensity_;
            std::vector<double> cars_;
            std::vector<double> carsYslice_;
            std::vector<int> directions_;

            // Random states
            int seed_;
            std::default_random_engine engine_;
            std::uniform_real_distribution<double> distribution_;
            
            // Player
            int playerX_;
            int playerY_;
            int playerSteps_;
            std::vector<double> fieldOfSight_;
            int nrInputNeurons_;
            std::vector< std::size_t > nrNeurons_;
            AI::NeuralNetwork neuralNetwork_;
            std::unordered_map<std::size_t,
                               std::tuple<std::size_t, std::size_t, std::size_t> > genomeHash_;
            
            // =========
            // Functions
            // =========
            
            // Compute a flattened 1d index from 2 indices
            int index2Dto1D(int ix, int iy);
            
            // Apply periodic boundary conditions to number i
            // with periodicity n
            int backToBox(int i, int n);
            
            void getFieldOfSight_();

    };

}

#endif
