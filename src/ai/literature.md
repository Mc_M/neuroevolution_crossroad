* https://www.tensorflow.org/tutorials/
* https://www.tensorflow.org/tutorials/recurrent
* https://www.tensorflow.org/tutorials/pdes
* https://iamtrask.github.io/2015/07/12/basic-python-network/
* https://cognitivedemons.wordpress.com/2017/07/06/a-neural-network-in-10-lines-of-c-code/
* Activation functions: https://medium.com/the-theory-of-everything/understanding-activation-functions-in-neural-networks-9491262884e0
  and https://www.tensorflow.org/versions/r0.12/api_docs/python/nn/activation_functions_.
* http://www.wildml.com/2015/09/implementing-a-neural-network-from-scratch/
* http://www.rueckstiess.net/snippets/show/17a86039

