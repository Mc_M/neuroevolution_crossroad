#include "../../include/neuralnetwork.h"
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>

AI::NeuralNetwork::NeuralNetwork() {
    // This is a dummy constructor
    std::vector< std::size_t > dummy_ = {1};
    NeuralNetwork(dummy_, ACTIVATION_LINEAR, false, false);
}

AI::NeuralNetwork::NeuralNetwork(std::vector<std::size_t> nrNeurons, int activationFunctionMode,
                                 bool verbose, bool debug) {
    if (verbose) {
        std::cout << "NeuralNetwork created ( [ ";
        for (std::size_t nr : nrNeurons) {
            std::cout << nr << " ";
        }
        std::cout << "] ";
        std::cout << activationFunctionMode << " ";
        std::cout << verbose << " " << debug << " )" << std::endl;
    }
    // Settings
    verbose_ = verbose;
    debug_  = debug;
    // Topology
    nrLayers_ = nrNeurons.size();
    nrNeurons_ = nrNeurons;
    // Prepare neurons
    neurons_.resize(nrLayers_);
    for (std::size_t iLayers=0; iLayers<nrLayers_; iLayers++) {
        neurons_[iLayers] = std::make_unique< Eigen::VectorXd >(nrNeurons_[iLayers]+1);
        for (std::size_t index=0; index<nrNeurons_[iLayers]+1; index++) {
            (*(neurons_[iLayers]))[index] = 0.0;
        }
    }
    // Prepare weights
    weights_.resize(nrLayers_-1);
    for (std::size_t iLayers=0; iLayers<nrLayers_-1; iLayers++) {
        weights_[iLayers] = std::make_unique< Eigen::MatrixXd >(nrNeurons_[iLayers+1]+1, nrNeurons_[iLayers]+1);
        for (std::size_t index1=0; index1<nrNeurons_[iLayers+1]+1; index1++) {
            for (std::size_t index2=0; index2<nrNeurons_[iLayers]+1; index2++) {
                (*(weights_[iLayers]))(index1, index2) = 0.0;
            }
        }
    }
    // Activation function
    if (activationFunctionMode==ACTIVATION_LINEAR) {
        activationFunction_ = activationLinear;
    } else if (activationFunctionMode==ACTIVATION_SIGMOID) {
        activationFunction_ = activationSigmoid;
    } else if (activationFunctionMode==ACTIVATION_TANH) {
        activationFunction_ = activationTanh;
    } else if (activationFunctionMode==ACTIVATION_RELU) {
        activationFunction_ = activationRelu;
    }
};

void AI::NeuralNetwork::setInputNeurons(std::size_t index, double value) {
    assert(index>0 and index<(nrNeurons_[0]+1) && "Invalid input neuron index.");
    (*(neurons_[0]))[index] = value;
}

double AI::NeuralNetwork::getOutputNeurons(std::size_t index) {
    assert(index>0 and index<(nrNeurons_[nrLayers_-1]+1) && "Invalid output neuron index.");
    return (*(neurons_[nrLayers_-1]))[index];
}

double AI::NeuralNetwork::getNeuronValue(std::size_t layer, std::size_t index) {
    assert(layer>=0 and layer<(nrLayers_)           && "Invalid layer index.");
    assert(index>=0 and index<(nrNeurons_[layer]+1) && "Invalid neuron index.");
    return (*(neurons_[layer]))[index];
}

void AI::NeuralNetwork::setWeight(std::size_t layer, std::size_t endNeuron, std::size_t startNeuron, double value) {
    assert(layer>=0       and layer      <(nrLayers_-1)           && "Invalid layer index.");
    assert(startNeuron>=0 and startNeuron<(nrNeurons_[layer]  +1) && "Invalid start neuron index.");
    assert(endNeuron>=0   and endNeuron  <(nrNeurons_[layer+1]+1) && "Invalid start neuron index.");
    (*(weights_[layer]))(endNeuron, startNeuron) = value;
}

double AI::NeuralNetwork::getWeight(std::size_t layer, std::size_t endNeuron, std::size_t startNeuron) {
    assert(layer>=0       and layer      <(nrLayers_-1)           && "Invalid layer index.");
    assert(startNeuron>=0 and startNeuron<(nrNeurons_[layer]  +1) && "Invalid start neuron index.");
    assert(endNeuron>=0   and endNeuron  <(nrNeurons_[layer+1]+1) && "Invalid start neuron index.");
    return (*(weights_[layer]))(endNeuron, startNeuron);
}

void AI::NeuralNetwork::feedForward() {
    for (std::size_t l=0; l<nrLayers_-1; l++) {
        // Set bias neuron to 1
        (*(neurons_[l]))[0] = 1.0;
        // Multiply weights times neurons
        (*(neurons_[l+1])) = (*(weights_[l]))*(*(neurons_[l]));
        // Apply activation function
        for (std::size_t index=0; index<(nrNeurons_[l+1]+1); index++) {
            (*(neurons_[l+1]))[index] = activationFunction_((*(neurons_[l+1]))[index]);
        }
    }
    // Set output layer's bias neuron to 1
    (*(neurons_[nrLayers_-1]))[0] = 1.0;
}

int AI::NeuralNetwork::numberOfNeurons() {
    int result = 0;
    for(std::size_t l=0; l<neurons_.size(); l++) {
        result += (*(neurons_[l])).size()-1;
    }
    return result;
}

int AI::NeuralNetwork::numberOfWeights() {
    int result = 0;
    for(std::size_t l=0; l<weights_.size(); l++) {
        result += (*(weights_[l])).size();
    }
    return result;
}

void AI::NeuralNetwork::saveWeights(const std::string& path) {
    std::ofstream ofile;
    ofile.open(path);
//     ofile << "# layer endNeuron startNeuron value\n";
    for (std::size_t l=0; l<weights_.size(); l++) {
        Eigen::MatrixXd W = (*(weights_[l]));
        ofile << "# l = " << l << "\n";
        ofile << W << "\n\n";
    }
    ofile.close();
}

// ========================= Activation functions ==============================

double AI::NeuralNetwork::activationLinear(double in) {
    return in;
}

double AI::NeuralNetwork::activationSigmoid(double in) {
    return 1.0/(1.0+std::exp(-1.0*in));
}

double AI::NeuralNetwork::activationTanh(double in) {
    return std::tanh(in);
}

double AI::NeuralNetwork::activationRelu(double in) {
    return std::max(0.0, in);
}
