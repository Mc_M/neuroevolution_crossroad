#include "../../include/world.h"
#include <iostream>
#include <fstream>
#include <algorithm>

Frogger::World::World(int nx, int ny,
                      int sx, int sy,
                      int tMax,
                      double carDensity,
                      std::vector< std::size_t > topology,
                      int activation,
                      int seed
                     ) {
    
    std::cout << "World created (";
    std::cout << nx << " " << ny
                    << " " << sx
                    << " " << sy
                    << " " << tMax
                    << " " << carDensity
                    << " " << seed
                    << ") " << std::endl;
    
    // Save private data
    nx_ = nx;
    ny_ = ny;
    sx_ = sx;
    sy_ = sy;
    tMax_ = tMax;
    carDensity_ = carDensity;
    seed_ = seed;
    area_ = nx_*ny_;
    
    // TODO: Abort if sx, sy is too large for given nx, ny!
    
    // Initialise time
    t_ = 0;
    
    // Initialise random number generator
    engine_.seed(seed_);
    distribution_ = std::uniform_real_distribution<double>(0.0, 1.0);
    
    // Determine car directions randomly
    directions_.resize(ny_);
    for (int i=0; i<ny_; i++) {
        if (distribution_(engine_) < 0.5) {
            directions_[i] = CAR_LEFT;
        } else {
            directions_[i] = CAR_RIGHT;
        }
    }
    
    // Fill board randomly
    carsYslice_.resize(nx_);
    cars_.resize(area_);
    for (int i=0; i<area_; i++) {
        if (distribution_(engine_) < carDensity_) {
            cars_[i] = CAR_FULL;
        } else {
            cars_[i] = CAR_EMPTY;
        }
    }
    
    // Initialise player
    playerX_ = nx_/2;
    playerY_ = 0;
    playerSteps_ = 0;
    nrInputNeurons_ = (2*sx_+1)*(2*sy_+1)-1;
    fieldOfSight_.resize(nrInputNeurons_);
    // TODO: Ensure that it doesn't sit where a car will be soon
    
    // Initialise neural network
    nrNeurons_.push_back(nrInputNeurons_);
    for (std::size_t nn : topology) {
        nrNeurons_.push_back(nn);
    }
    nrNeurons_.push_back(5);
    neuralNetwork_ = AI::NeuralNetwork(nrNeurons_, activation, true, false);
    
    // Set up genome hash
    std::size_t ctrHash = 0;
    for (std::size_t iLayers=0; iLayers<nrNeurons_.size()-1; iLayers++) {
        for (std::size_t endNeuron=0; endNeuron<nrNeurons_[iLayers+1]+1; endNeuron++) {
            for (std::size_t startNeuron=0; startNeuron<nrNeurons_[iLayers]+1; startNeuron++) {
                genomeHash_[ctrHash] = std::make_tuple(iLayers, endNeuron, startNeuron);
                ctrHash += 1;
            }
        }
    }
    
}

int Frogger::World::index2Dto1D(int ix, int iy) {
    return nx_*iy+ix;
}

void Frogger::World::renderData(const std::string& path, const std::string& suffix) {
    
    std::ofstream ofile;
    ofile.open(path+"/world.t"+std::to_string(t_)+"_"+suffix+".dat");
    
    ofile << "# nx " << nx_ << "\n";
    ofile << "# ny " << ny_ << "\n";
    ofile << "# t " << t_ << "\n";
    
    int i1d = 0;
    for (int iy=ny_-1; iy>=0; iy--) {
        for (int ix=0; ix<nx_; ix++) {
            i1d = index2Dto1D(ix, iy);
            if (ix == playerX_ and iy == playerY_) {
                ofile <<  "[" << cars_[i1d] << "]"  << " ";
            } else {
                ofile <<  " " << cars_[i1d] << " "  << " ";
            }
        }
        ofile << "| " << directions_[iy] << "\n";
    }
    
    ofile.close();
}

int Frogger::World::backToBox(int i, int n) {
    if (i >= 0) {
        return i % n;
    } else {
        return (n + (i%n)) % n;
    }
}

void Frogger::World::advanceCars() {
    for (int iy=0; iy<ny_; iy++) {
        // Copy data to vector (TODO: This could be optimised!)
        for (int ix=0; ix<nx_; ix++) {
            carsYslice_[ix] = cars_[index2Dto1D(ix, iy)];
        }
        // Rotate data
        if (directions_[iy] == CAR_LEFT) {
            std::rotate(carsYslice_.begin(), carsYslice_.begin()+1, carsYslice_.end());
        } else {
            std::rotate(carsYslice_.rbegin(), carsYslice_.rbegin()+1, carsYslice_.rend());
        }
        // Copy data back (TODO: Part of above optimisation)
        for (int ix=0; ix<nx_; ix++) {
            cars_[index2Dto1D(ix, iy)] = carsYslice_[ix];
        }
    }
}

void Frogger::World::advancePlayer() {
    // Make decision
    // TODO: Use Neural Network class
    getFieldOfSight_();
    for (std::size_t i=0; i<nrInputNeurons_; i++) {
        neuralNetwork_.setInputNeurons(i+1, fieldOfSight_[i]);
    }
    neuralNetwork_.feedForward();
    int decision = PLAYER_UP;
    for (int i=1; i<5; i++) {
        if (neuralNetwork_.getOutputNeurons(i+1)>neuralNetwork_.getOutputNeurons(decision+1)) {
            decision = i;
        }
    }
std::cout << decision << " - ";
for (std::size_t i=0; i<6; i++) {
    std::cout << neuralNetwork_.getNeuronValue(nrNeurons_.size()-1, i) << " ";
}
std::cout << std::endl;
//     decision = (playerSteps_/5)%5;
    // Take step
    switch (decision) {
        case PLAYER_UP:
            playerX_ += 0;
            playerY_ += 1;
            break;
        case PLAYER_DOWN:
            playerX_ += 0;
            playerY_ += -1;
            break;
        case PLAYER_LEFT:
            playerX_ += -1;
            playerY_ += 0;
            break;
        case PLAYER_RIGHT:
            playerX_ += 1;
            playerY_ += 0;
            break;
        case PLAYER_STAY:
            playerX_ += 0;
            playerY_ += 0;
            break;
    }
    playerX_ = backToBox(playerX_, nx_);
    playerY_ = backToBox(playerY_, ny_);
    playerSteps_ += 1;
}

void Frogger::World::advance() {
    advancePlayer();
    advanceCars();
    t_ += 1;
}

bool Frogger::World::isPlayerAlive() {
    bool result;
    if (cars_[index2Dto1D(playerX_, playerY_)] == CAR_FULL) {
        result = false;
    } else {
        result = true;
    }
    return result;
    // TODO: Declare a situation where the player jumps over a car (e.g.
    // car goes to the left while player goes to the right while they are
    // adjacent to each other) as a dead player. This is not yet captured
    // with this implementation.
}

// Fill fieldOfSight_ from board using player's current position.
void Frogger::World::getFieldOfSight_() {
    int ctr = 0;
    int ixInBox = -1;
    int iyInBox = -1;
    for (int ix=playerX_-sx_; ix<playerX_+sx_+1; ix++) {
        for (int iy=playerY_-sy_; iy<playerY_+sy_+1; iy++) {
            ixInBox = backToBox(ix, nx_);
            iyInBox = backToBox(iy, ny_);
            if (not (ixInBox == playerX_ and iyInBox == playerY_)) {
                fieldOfSight_[ctr] = directions_[iyInBox]*(cars_[index2Dto1D(ixInBox, iyInBox)]-0.0);
                
// Maybe the features must be modified to work properly?
// fieldOfSight_[ctr] = fieldOfSight_[ctr]+1.1;
// // -1, 0, +1
// // transform to: x -> x+1.1 => 0.1, 1.1, 2.1
                
                // TODO: Modify this computation to use better features that are fed to the neural net.
                ctr += 1;
            }
        }
    }
}

// ==================
// Invidivual methods
// ==================

std::size_t Frogger::World::genomeLength() {
    return neuralNetwork_.numberOfWeights();
}

void Frogger::World::setGene(int index, double gene) {
    std::tuple<std::size_t, std::size_t, std::size_t> indexPair = genomeHash_[index];
    std::size_t layer       = std::get<0>(indexPair);
    std::size_t endNeuron   = std::get<1>(indexPair);
    std::size_t startNeuron = std::get<2>(indexPair);
    neuralNetwork_.setWeight(layer, endNeuron, startNeuron, gene);
}

double Frogger::World::getGene(int index) {
    std::tuple<std::size_t, std::size_t, std::size_t> indexPair = genomeHash_[index];
    std::size_t layer       = std::get<0>(indexPair);
    std::size_t endNeuron   = std::get<1>(indexPair);
    std::size_t startNeuron = std::get<2>(indexPair);
    return neuralNetwork_.getWeight(layer, endNeuron, startNeuron);
}
