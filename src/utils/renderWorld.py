"""
Render frames from world data.
"""

import argparse
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import os
import numpy as np

# CLI
parser = argparse.ArgumentParser(description="Render frames from world data.")
parser.add_argument("-files", nargs="*",
                    help="Data files to process.")
parser.add_argument("-outdir", required=False, default="./",
                    help="Output directory. Default: ./")
args = parser.parse_args()
files = args.files
outdir = args.outdir

l = len(files)
ctr = 0

for rel_path in files:
    
    abs_path_in = os.path.abspath(rel_path)
    abs_path_out = os.path.join(outdir, rel_path.replace(".dat", ".png"))
    
    lines = []
    with open(abs_path_in, "r") as f:
        for line in f:
            lines.append(line.replace("\n", ""))
    
    nx = int(lines[0].split()[-1])
    ny = int(lines[1].split()[-1])
    t = int(lines[2].split()[-1])
    
    data = np.zeros((nx, ny), dtype=int)
    playerX = -1
    playerY = -1
    val = -1
    for iy, line in enumerate(lines[3:]):
        for ix, element in enumerate(line.split()[:-2]):
            if "[" in element:
                playerX = ix
                playerY = iy
                val = int(element.replace("[", "").replace("]", ""))
            else:
                val = int(element)
            data[ix, iy] = val
            
    plt.clf()
            
    circle = Circle((playerX, playerY), 0.4, facecolor="red")
    plt.gca().add_artist(circle)
    
    plt.imshow(data.transpose(), cmap="Greys")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.yticks(np.arange(ny))
    # TODO: The y labels must be reversed!
    plt.xticks(np.arange(nx))
    plt.title("t={}".format(t))
    plt.savefig(abs_path_out)

    ctr += 1
    print("Processed: {} of {}".format(ctr, l))
