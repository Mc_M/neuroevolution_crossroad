1. Build the class that implements the Individual interface.
2. Add them to population on which the Algo will operate.
3. Feed that population to the Algo. Also some structure that
   is used for breeding the offspring
		NOT CLEAR HOW TO DO THAT
4. Evolve the population
	a) Track statistics while doing so
5. Get the final population. This population is the same as the
   one in the beginning holding the same objects.

THUS:
x) The algo operates mutable Individual objects because copying
   is not easy when using interfaces and also an Individual might
   be an expensive object.
